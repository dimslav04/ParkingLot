package com.example.user.parkinglot;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;


public class SaveActivity extends AppCompatActivity implements LocationListener {
    SharedPreferences.Editor myEditor;
    SharedPreferences myPersistFile;
    MyDatabaseHelper mDbHelper;
    SQLiteDatabase dbRead;
    SQLiteDatabase dbWrite;
    EditText etID;
    CheckBox myCheckBox;
    int is_Favor;
    int rating;
    RatingBar rb;
    WebView myWeb;
    TextView myTextView;
    LocationManager myLocationManager;
    String locationLat,nameLot;
    String locationLon;
    Location firstLocation;
    public static final String FAVOR_LOT = "com.example.user.parkinglot.favor";
    public static final String LOTRATING = "com.example.user.parkinglot.lotRating";
    public static final String NAME_LOT = "com.example.user.parkinglot.nameLot";
    public static final String FIRST_LATITUDE = "com.example.user.parkinglot.latitude";
    public static final String FIRST_LONGITUDE = "com.example.user.parkinglot.longitude";
    MediaPlayer myPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);
        myPlayer = MediaPlayer.create(this, R.raw.cash_sound);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPersistFile = getSharedPreferences(SettingActivity.FIRST_SHARED_FILE, 0);
        myEditor = myPersistFile.edit();
        myEditor.commit();
        myTextView = findViewById(R.id.textViewLocation);
        mDbHelper = new MyDatabaseHelper(this);
        dbWrite = mDbHelper.getWritableDatabase();
        etID = findViewById(R.id.LotNameEdit);
        rb=findViewById(R.id.rating1);

        myCheckBox = findViewById(R.id.checkBoxFavor);
        is_Favor=myPersistFile.getInt(SaveActivity.FAVOR_LOT,0);
        myCheckBox.setChecked(is_Favor == 1);
        nameLot="";
        rating=myPersistFile.getInt(SaveActivity.LOTRATING,1);
        dbRead = mDbHelper.getReadableDatabase();
        myWeb = findViewById(R.id.webViewLocation);
        myLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

        myCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    is_Favor = 1;
                else
                    is_Favor = 0;
                saveRating();
            } });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

         rb.setRating((float)rating);
        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
           @Override
              public void onRatingChanged(RatingBar myRatingBar, float x, boolean fromUser)
                {
                    rating=(int)x;
                     saveRating ();
                }
        }
        );
        etID.setText(nameLot);
        myLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3 * 1000, 10, this);
        firstLocation = myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
      if(firstLocation==null){
          firstLocation=new Location("");
      locationLat =myPersistFile.getString(SaveActivity.FIRST_LATITUDE,"34");
      locationLon=myPersistFile.getString(SaveActivity.FIRST_LONGITUDE,"33"); }
      etID.setText("");
      locationLat=Double.toString(firstLocation.getLatitude());
      locationLon=Double.toString(firstLocation.getLongitude());
        myWeb.setWebViewClient(new WebViewClient());
        WebSettings settings1=myWeb.getSettings();
        settings1.setJavaScriptEnabled(true);
        setLocWeb();
        saveRating();
    }
public void saveRating(){
      myEditor.putString(FIRST_LATITUDE, Double.toString(firstLocation.getLatitude()));
      myEditor.putString(FIRST_LONGITUDE,  Double.toString(firstLocation.getLongitude()));
    myEditor.putInt(LOTRATING, rating);
    myEditor.putInt(FAVOR_LOT,is_Favor);
    myEditor.putString(NAME_LOT,etID.getText().toString());
    myEditor.putString("lastActivity", Saved2Activity.class.getName());
    myEditor.commit();
}



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        myEditor.putString("lastActivity", StoryActivity.class.getName());
        myEditor.commit();
        Intent nextActivity;

        if (id == R.id.action_home) {
            nextActivity=new Intent(this,MainActivity.class);
        }
        else if(id == R.id.action_save){
            return true;
        }
        else if(id == R.id.action_settings){

            nextActivity=new Intent(this,SettingActivity.class);
        }

        else if(id==R.id.action_story){
            nextActivity=new Intent(this,StoryActivity.class);
        }
        else if (id == R.id.action_team) {
            nextActivity=new Intent(this,TeamActivity.class);
        }

        else nextActivity=new Intent(this,NearActivity.class);

        startActivity(nextActivity);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
       saveRating();
    }

    @Override
    protected void onStop() {
        super.onStop();

        saveRating();
    }

    public void saveRaring1(View v) {
        saveRating1( );

    }
    private void  saveRating1( ){
      boolean b=inputsAreCorrect(etID.getText().toString());
      if(b){
        ContentValues values = new ContentValues();
        values.put(MyDatabaseContract.TableLots.COLUMN_NAME_LOT_NAME, etID.getText().toString());
        values.put(MyDatabaseContract.TableLots.COLUMN_NAME_IS_FAVORITEE, is_Favor);
        values.put(MyDatabaseContract.TableLots.COLUMN_NAME_ESTIMATE, rating);
        long id = dbWrite.insert(MyDatabaseContract.TableLots.TABLE_NAME, null, values);
        myEditor.putString(NAME_LOT,"");
          myEditor.putString(FIRST_LATITUDE, Double.toString(firstLocation.getLatitude()));
          myEditor.putString(FIRST_LONGITUDE,  Double.toString(firstLocation.getLongitude()));
          myEditor.putInt(LOTRATING, rating);
          myEditor.putInt(FAVOR_LOT,is_Favor);
          myEditor.putString("lastActivity", StoryActivity.class.getName());
        myEditor.commit();
          myPlayer.start();
        start();}
    }
public  void start(){
    Intent bakasha1 = new Intent(this, StoryActivity.class);
    startActivity(bakasha1);}

    private boolean inputsAreCorrect(String name) {
        if (name.trim().isEmpty()) {
            etID.setError(getString(R.string.enterNumber));
            etID.requestFocus();
            return false;
        }
        return true;
    }
    public void onLocationChanged(Location location) {
       //firstLocation = location; if want to get changed immediately
       // setLocWeb();
    }

    private void setLocWeb() {
        String strUrl = "https://www.google.com/maps/place/" + firstLocation.getLatitude() + "," + firstLocation.getLongitude();
        myWeb.loadUrl(strUrl);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
