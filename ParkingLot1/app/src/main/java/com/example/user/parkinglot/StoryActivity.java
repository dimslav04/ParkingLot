package com.example.user.parkinglot;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

public class StoryActivity extends AppCompatActivity  implements LocationListener  {


    WebView myWeb;
    TextView myTextView;
    TextView myTextViewCar;
    Location firstLocation;
    LocationManager myLocationManager;
    SharedPreferences settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);
          settings=getSharedPreferences(SettingActivity.FIRST_SHARED_FILE, 0);
        saveSettings();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myWeb = findViewById(R.id.webViewLocation);
        myTextViewCar = findViewById(R.id.textViewLocation2);
        myTextView = findViewById(R.id.textViewLocation);
        String defaultValue = ""; // getResources().getString(R.string.myResForNameOfmyFirstName);

        myTextView.setText(getString(R.string.fname_label)+" "+settings.getString(getString(R.string.myResForNameOfmyFirstName),defaultValue)+" "+ settings.getString(getString(R.string.myResForNameOfmyLastName),defaultValue));
        // defaultValue = getResources().getString(R.string.myResForNameOfmyCarType);
        myTextViewCar.setText(getString(R.string.TypeCar)+" "+settings.getString(getString(R.string.myResForNameOfmyCarType),defaultValue)+", "+getString(R.string.NumberCar)+": "+settings.getString(getString(R.string.myResForNameOfmyCarNumber),defaultValue));
        myLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        firstLocation=new Location("");
       String lat=settings.getString(SaveActivity.FIRST_LATITUDE,"34");
      String  lon=settings.getString(SaveActivity.FIRST_LONGITUDE,"33");
        firstLocation.setLongitude(Double.parseDouble(lon));
        firstLocation.setLatitude(Double.parseDouble(lat));
        myWeb.setWebViewClient(new WebViewClient());
        WebSettings setting=myWeb.getSettings();
        setting.setJavaScriptEnabled(true);
        String strUrl="https://www.google.com/maps/place/"+Double.parseDouble(lat)+","+Double.parseDouble(lon);
        myWeb.loadUrl(strUrl);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent nextActivity;
        saveSettings();
        if (id == R.id.action_story) {
            return true;
        }
        else if(id == R.id.action_near){
            nextActivity=new Intent(this,NearActivity.class);
        }
        else if(id == R.id.action_save){
            nextActivity=new Intent(this,SaveActivity.class);
        }
        else if(id==R.id.action_home){
            nextActivity=new Intent(this,MainActivity.class);
        }
        else if (id == R.id.action_team) {
            nextActivity=new Intent(this,TeamActivity.class);
        }

        else nextActivity=new Intent(this,SettingActivity.class);
        startActivity(nextActivity);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }
    @Override
    protected void onPause() {
        super.onPause();
        saveSettings();
    }

    @Override
    protected void onStop() {
        super.onStop();

        saveSettings();
    }
    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
    public void saveSettings(){
        SharedPreferences setting=getSharedPreferences(SettingActivity.FIRST_SHARED_FILE, 0);
        SharedPreferences.Editor myEditor = setting.edit();
        myEditor.putString("lastActivity", getClass().getName());
        myEditor.commit();
    }
}
