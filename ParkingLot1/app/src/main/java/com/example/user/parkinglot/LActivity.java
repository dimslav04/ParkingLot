package com.example.user.parkinglot;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LActivity extends AppCompatActivity {
    SharedPreferences prefs;
    Class<?> activityClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = getSharedPreferences(SettingActivity.FIRST_SHARED_FILE, 0);

        try {
            activityClass = Class.forName( prefs.getString("lastActivity", SettingActivity.class.getName()));
            SharedPreferences.Editor myEditor = prefs.edit();
            myEditor.putString("lastActivity", activityClass.getName());

        }
        catch(ClassNotFoundException ex) {activityClass = SettingActivity.class;}
        startActivity(new Intent(this, activityClass));

    }
    @Override
    protected void onRestart() {
        super.onRestart();
    }



}
