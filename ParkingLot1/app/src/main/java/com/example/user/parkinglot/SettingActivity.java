package com.example.user.parkinglot;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class SettingActivity extends AppCompatActivity implements LocationListener{
    public static final  String FIRST_SHARED_FILE="com.example.user.parkinglot.myFirstPersistentObjectName";
    EditText fin;
    EditText ln;
    EditText cn;
    EditText tc;
    Intent nextActivity;
    SharedPreferences settings;
    SharedPreferences.Editor myEditor;
    MediaPlayer myPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myPlayer = MediaPlayer.create(this, R.raw.cash_sound);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        ln = findViewById(R.id.lName);
        tc = findViewById(R.id.typeCar);
        cn = findViewById(R.id.NumberCar);
        fin = findViewById(R.id.fName);
        settings=getSharedPreferences(FIRST_SHARED_FILE, 0);
        myEditor = settings.edit();
        String defaultValue = "";
        fin.setText(settings.getString(getString(R.string.myResForNameOfmyFirstName),defaultValue));
        ln.setText(settings.getString(getString(R.string.myResForNameOfmyLastName),defaultValue));
        cn.setText(settings.getString(getString(R.string.myResForNameOfmyCarNumber),defaultValue));
        tc.setText(settings.getString(getString(R.string.myResForNameOfmyCarType),defaultValue));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_home) {
            nextActivity=new Intent(this,MainActivity.class);
        }
        else if(id == R.id.action_settings){
            return true;
        }
        else if(id == R.id.action_save){
            nextActivity=new Intent(this,SaveActivity.class);
        }
        else if(id==R.id.action_story){
            nextActivity=new Intent(this,StoryActivity.class);
        }
        else if (id == R.id.action_team) {
            nextActivity=new Intent(this,TeamActivity.class);
        }

        else nextActivity=new Intent(this,NearActivity.class);
        saveRating();
        startActivity(nextActivity);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveRating();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveRating();}

    public void  saveMethod(View v){
        saveRating();

        Intent   nextActivity = new Intent(this, SaveActivity.class);
        startActivity(nextActivity);
    }
    private boolean input(){
        return inputsAreCorrect(fin.getText().toString(), fin) && inputsAreCorrect(ln.getText().toString(), ln) &&
                inputsAreCorrect(cn.getText().toString(), cn) && inputsAreCorrect(tc.getText().toString(), tc);
    }

    private void saveRating() {
        if (input() ){
            myPlayer.start();
            myEditor.putString("lastActivity", StoryActivity.class.getName());
            myEditor.putString(getString(R.string.myResForNameOfmyFirstName), fin.getText().toString());
            myEditor.putString(getString(R.string.myResForNameOfmyLastName), ln.getText().toString());
            myEditor.putString(getString(R.string.myResForNameOfmyCarType), tc.getText().toString());
            myEditor.putString(getString(R.string.myResForNameOfmyCarNumber), cn.getText().toString());
            myEditor.commit();

        }
    }
    private boolean inputsAreCorrect(String name,EditText editText) {
        if (name.trim().isEmpty()) {
            editText.setError(getString(R.string.enterNumber));
            editText.requestFocus();
            return false;
        }
        return true;
    }

}
