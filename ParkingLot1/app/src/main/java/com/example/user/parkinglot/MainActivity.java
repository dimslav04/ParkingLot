package com.example.user.parkinglot;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity  {
    TextView textView;
    SharedPreferences setting;
    MyDatabaseHelper mDbHelper;
    SQLiteDatabase dbRead;
    SQLiteDatabase dbWrite;
    String lon;
    String lat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDbHelper = new MyDatabaseHelper(this);
        dbWrite = mDbHelper.getWritableDatabase();
        dbRead = mDbHelper.getReadableDatabase();
         textView = findViewById(R.id.textView);

        setting=getSharedPreferences(SettingActivity.FIRST_SHARED_FILE, 0);
        lat=setting.getString(SaveActivity.FIRST_LATITUDE,"34");
        lon=setting.getString(SaveActivity.FIRST_LONGITUDE,"33");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

    }
    public void readData(View view)
    {
        readData(   );}
    public void readData(){
    TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("");
    Cursor cursor = dbWrite.query(MyDatabaseContract.TableLots.TABLE_NAME, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
        String name = cursor.getString(1);
        int favor = cursor.getInt(2);
        int rate=cursor.getInt(3);
        double lati=Double.parseDouble(lat);
            double loni=Double.parseDouble(lon);

        textView.append(getString(R.string.fname_label)+" "+ name +", "+ getString(R.string.rate)+": " + rate + favor(favor)+"\n");
    }
        cursor.close();

}

    public void deletData(View v){
        dbRead.delete(MyDatabaseContract.TableLots.TABLE_NAME, null, null);
        SharedPreferences.Editor myEditor = setting.edit();
         readData( );
    }
    private String favor(int s){
        String str=", ";
        if(s==0){ str+=getString(R.string.no);
        str+=" ";}
        return str+=getString(R.string.food);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent nextActivity;

        if (id == R.id.action_home) {
            return true;
        }
        else if(id == R.id.action_near){
            nextActivity=new Intent(this,NearActivity.class);
        }
        else if(id == R.id.action_save){
            nextActivity=new Intent(this,SaveActivity.class);
        }
        else if(id==R.id.action_story){
            nextActivity=new Intent(this,StoryActivity.class);
        }
        else if (id == R.id.action_team) {
            nextActivity=new Intent(this,TeamActivity.class);
        }

        else nextActivity=new Intent(this,SettingActivity.class);
        startActivity(nextActivity);
        return super.onOptionsItemSelected(item);
    }
}
