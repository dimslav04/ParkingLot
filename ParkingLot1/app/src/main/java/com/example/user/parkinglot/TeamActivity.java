package com.example.user.parkinglot;

import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class TeamActivity extends AppCompatActivity implements LocationListener{
    VideoView myVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        myVideo=findViewById(R.id.VideoView);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Uri tempURI= Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.dima);
        myVideo.setVideoURI(tempURI);
        myVideo.setMediaController(new MediaController(this));
        myVideo.requestFocus();
        myVideo.start();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent nextActivity;

        if (id == R.id.action_home) {
            nextActivity=new Intent(this,MainActivity.class);
        }
        else if(id == R.id.action_team){
            return true;
        }
        else if(id == R.id.action_save){
            nextActivity=new Intent(this,SaveActivity.class);
        }
        else if(id == R.id.action_settings){

            nextActivity=new Intent(this,SettingActivity.class);
        }
        else if(id==R.id.action_story){
            nextActivity=new Intent(this,StoryActivity.class);
        }

        else nextActivity=new Intent(this,NearActivity.class);

        startActivity(nextActivity);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
